/*
 * @category BushFire
 * @copyright Copyright (C) 2016 BushFire. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.rakki.video.edit.example;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

/**
 * This class used to handle the media permissions
 *
 * @author BushFire Team
 */

public class MediaPermissions {

    /**
     * Empty constructor
     */
    public MediaPermissions() {
        // Empty constructor
    }

    /**
     * Calling this method to check the permission status
     *
     * @param context    Context of the activity
     * @param permission Permission to ask
     * @return boolean True if grand the permission
     */
    public boolean isPermissionAllowed(Context context, String permission) {
        /**
         * Getting the permission status
         */
        int result = ContextCompat.checkSelfPermission(context, permission);

        /**
         * If permission is granted returning true
         */
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        /**
         * If permission is not granted returning false
         */
        return false;
    }

    /**
     * Request the camera permission from the camera chosen for take photo
     *
     * @param activity       Activity of the View
     * @param permissionCode Code for start activity
     */
    public void requestPermissions(Activity activity, int permissionCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
            Toast.makeText(activity,"With out this permission will not work ",Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(activity,"With out this permission will not work ",Toast.LENGTH_LONG).show();
        }
        /**
         * And finally ask for the permission
         */
        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
               }, permissionCode);
    }

    /**
     * Redirect to app settings screen to ask permissions for app
     *
     * @param context Application context
     */
 /*   public void redirectToPermissionRequest(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", Constants.PACKAGE_NAME, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        Toast.makeText(context, "Enable permissions in app settings",Toast.LENGTH_LONG).show();
    }*/
}
